'use strict';
module.exports = (sequelize, DataTypes) => {
  const Barangay = sequelize.define('Barangay', {
    barangayName: DataTypes.STRING
  }, {});
  Barangay.associate = function(models) {
    Barangay.hasMany(models.Precinct);
    Barangay.hasMany(models.Voter);
  };
  return Barangay;
};