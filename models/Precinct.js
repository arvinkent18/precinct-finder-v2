'use strict';
module.exports = (sequelize, DataTypes) => {
  const Precinct = sequelize.define('Precinct', {
    precinctId: DataTypes.STRING,
    barangayId: DataTypes.INTEGER
  }, {});
  Precinct.associate = function(models) {
    Precinct.hasMany(models.Voter);
    Precinct.belongsTo(models.Barangay);
  };
  return Precinct;
};