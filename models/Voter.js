'use strict';
module.exports = (sequelize, DataTypes) => {
  const Voter = sequelize.define('Voter', {
    barangayId: DataTypes.INTEGER,
    precinctId: DataTypes.INTEGER,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    address: DataTypes.TEXT
  }, {});
  Voter.associate = function(models) {
    Voter.belongsTo(models.Precinct, { foreignKey: 'precinctId' });
    Voter.belongsTo(models.Barangay, { foreignKey: 'barangayId' });
  };
  return Voter;
};