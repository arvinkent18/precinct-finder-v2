const express = require("express");
const router = express.Router();
const Precinct = require("../models").Precinct;
const Barangay = require("../models").Barangay;
const Voter = require("../models").Voter;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// Retreiving the list of Barangays with each Precincts and Voters
router.get("/", (req, res) => {
  Barangay.findAll({
    include: [
      {
        model: Precinct,
        include: [Voter]
      }
    ],
    order: ["barangayName"]
  }).then(barangays => {
    res.render("index", { barangays: barangays });
  });
});

// Creating of new Precinct
router.post("/precinct/:id", (req, res) => {
  Precinct.create({ ...req.body, barangayId: parseInt(req.params.id) }).then(
    () => res.redirect("/")
  );
});

// Creating of new Barangay
router.post("/barangay", (req, res) => {
  Barangay.create(req.body).then(() => res.redirect("/"));
});

// Creating of new Voter
//
// @params bid
// @params pid
router.post("/voter/:bid/:pid", (req, res) => {
  Voter.create({
    ...req.body,
    barangayId: parseInt(req.params.bid),
    precinctId: req.params.pid
  }).then(() => res.redirect("/"));
});

// Searching of Precinct based on the name of the Voter
//
// @params firstName
// @params lastName
router.get("/search/voter", (req, res) => {
  Voter.findAll({
    include: [Precinct],
    where: {
      firstName: req.query.firstName,
      lastName: req.query.lastName
    }
  }).then(voter => {
    if (voter.length > 0) {
      let voterData = {
        firstName: voter[0].firstName,
        lastName: voter[0].lastName,
        middleName: voter[0].middleName,
        address: voter[0].address,
        precinctId: voter[0]["Precinct"].precinctId
      };
      voterJSON = JSON.stringify(voterData);
      res.send(voterJSON);
    } else {
      res.send("No Data");
    }
  });
});

module.exports = router;
