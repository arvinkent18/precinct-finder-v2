$(document).ready(function() {
  // Captures the Barangay ID and Precinct ID
  $(".new-voter").on("click", function(e) {
    $target = $(e.target);
    const bid = $target.attr("data-bid");
    const pid = $target.attr("data-pid");

    // Creates new voter
    $("#voter-form").on("submit", function(e) {
      $.ajax({
        type: "POST",
        url: "/voter/" + bid + "/" + pid,
        data: $(this).serialize(),
        success: function() {
          console.log("Successfully added new voter");
          window.location.href = "/";
        },
        error: function(err) {
          console.log(err);
        }
      });
      e.preventDefault();
    });
  });

  // Searches for the precinct number based on the name of the person
  $("#voter-search").on("submit", function(e) {
    $.ajax({
      type: "GET",
      url: "/search/voter",
      data: $(this).serialize(),
      success: function(data) {
        if (data == "No Data") {
          // HTML Output
          htmlOutput =
            '<div class="container col-md-12 text-center alert alert-danger">';
          htmlOutput +=
            '<h2 class="display-6">Voter not found in the database!</h2>';
          htmlOutput += "</div>";

          // Toggle Search Result Modal
          $("#searchResultModal").modal();

          // Appends the HTML output for the Voter's detail
          $("#result-content").append(htmlOutput);
        } else {
          // Parse the voter response
          let voter = JSON.parse(data);
          htmlOutput = '<div class="container">';
          htmlOutput += '<div class="col-md-12">';
          htmlOutput +=
            '<h6 class="display-6"><i class="fa fa-user"></i>Voter: ' +
            voter["lastName"] +
            " " +
            voter["firstName"] +
            " " +
            voter["middleName"] +
            "</h6>";
          htmlOutput +=
            '<h6 class="display-6"><i class="fa fa-home"></i>Address: ' +
            voter["address"] +
            "</h6>";
          htmlOutput +=
            '<h6 class="display-6"><i class="fa fa-building"></i>Precinct No: ' +
            voter["precinctId"] +
            "</h6>";
          htmlOutput += "</div>";
          htmlOutput += "</div>";

          // Toggle Search Result Modal
          $("#searchResultModal").modal();

          // Appends the HTML output for the Voter's detail
          $("#result-content").append(htmlOutput);
        }

        // Removes the content of Search Result Modal upon modal close
        $("#search-close").on("click", function(e) {
          $("#result-content")
            .children()
            .remove();
          $("#voter-search")[0].reset();
        });
      }
    });
    e.preventDefault();
  });
});
